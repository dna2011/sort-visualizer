import { v4 as uuidv4 } from 'uuid';

export function generateArray(size, min = 1, max = 500) {
  const gen = [];
  for (let i = 0; i < parseInt(size); ++i) {
    gen.push({
      value: Math.floor(min + Math.random() * (max - min + 1)),
      id: uuidv4(),
    });
  }
  return gen;
}
